package it.ivanbr.lab1

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import kotlin.math.roundToInt

class ShowProfileActivity : AppCompatActivity() {
    private lateinit var iv : ImageView
    private lateinit var name_tv : TextView
    private lateinit var nickname_tv : TextView
    private lateinit var idCode_tv : TextView
    private lateinit var email_tv : TextView
    private lateinit var phone_tv : TextView
    private lateinit var location_tv : TextView

    /*
    val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
    val defaultValue = resources.getInteger(R.integer.saved_high_score_default_key)
    val highScore = sharedPref.getInt(getString(R.string.saved_high_score_key), defaultValue)*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_profile)

        iv = findViewById<ImageView>(R.id.photoView)
        name_tv = findViewById<TextView>(R.id.nameView)
        nickname_tv = findViewById<TextView>(R.id.nicknameView)
        idCode_tv = findViewById<TextView>(R.id.idCodeView)
        email_tv = findViewById<TextView>(R.id.emailView)
        phone_tv = findViewById<TextView>(R.id.phoneView)
        location_tv = findViewById<TextView>(R.id.locationView)

        //use shared preferences if they exist (or default values)
        val sharedPref = getSharedPreferences("profilePrefs", MODE_PRIVATE)
        val picturePref = getSharedPreferences("picturePrefs", MODE_PRIVATE)

        val profilePicture: String? = picturePref.getString("imageUri", null)
        if( profilePicture != null ) iv.setImageURI(profilePicture.toUri())

        val name: String? = sharedPref.getString("name", "Full Name")
        name_tv.text = name

        val nickname: String? = sharedPref.getString("nickname", "Nickname")
        nickname_tv.text = nickname

        val idCode: String? = sharedPref.getString("idCode", "IDXCODEXXXXX")
        idCode_tv.text = idCode

        val email: String? = sharedPref.getString("email", "email@address")
        email_tv.text = email

        val phone: String? = sharedPref.getString("phone", "+000000000000")
        phone_tv.text = phone

        val location: String? = sharedPref.getString("location", "Location")
        location_tv.text = location

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) outState.putString("group31.lab1.PROFILE_PICTURE", profilePicture)
        outState.putString("group31.lab1.FULL_NAME", name_tv.text.toString())
        outState.putString("group31.lab1.NICKNAME", nickname_tv.text.toString())
        outState.putString("group31.lab1.ID_CODE", idCode_tv.text.toString())
        outState.putString("group31.lab1.EMAIL", email_tv.text.toString())
        outState.putString("group31.lab1.PHONE", phone_tv.text.toString())
        outState.putString("group31.lab1.LOCATION", location_tv.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        var profilePicture: String? = savedInstanceState.getString("group31.lab1.PROFILE_PICTURE")
        if ( profilePicture != null ) iv.setImageURI(profilePicture.toUri())
        else {
            profilePicture = getSharedPreferences("picturePrefs", MODE_PRIVATE
            ).getString("imageUri", null)
            if (profilePicture != null) iv.setImageURI(profilePicture.toUri())
        }
        name_tv.text = savedInstanceState.getString("group31.lab1.FULL_NAME")
        nickname_tv.text = savedInstanceState.getString("group31.lab1.NICKNAME")
        idCode_tv.text = savedInstanceState.getString("group31.lab1.ID_CODE")
        email_tv.text = savedInstanceState.getString("group31.lab1.EMAIL")
        phone_tv.text = savedInstanceState.getString("group31.lab1.PHONE")
        location_tv.text = savedInstanceState.getString("group31.lab1.LOCATION")

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.edit_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        editProfile()
        return true
    }

    private fun editProfile(){
        //explicit intent
        val intent = Intent(this, EditProfileActivity::class.java)
        //intent decorators
        intent.also {
            val profilePicture: String? = getSharedPreferences("picturePrefs",
                    MODE_PRIVATE).getString("imageUri", null)
            if( profilePicture != null ) it.putExtra("group31.lab1.PROFILE_PICTURE", profilePicture)
            it.putExtra("group31.lab1.FULL_NAME", name_tv.text.toString())
            it.putExtra("group31.lab1.NICKNAME", nickname_tv.text.toString())
            it.putExtra("group31.lab1.ID_CODE", idCode_tv.text.toString())
            it.putExtra("group31.lab1.EMAIL", email_tv.text.toString())
            it.putExtra("group31.lab1.PHONE", phone_tv.text.toString())
            it.putExtra("group31.lab1.LOCATION", location_tv.text.toString())
        }
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            val uri : String? = data?.getStringExtra("group31.lab1.PROFILE_PICTURE")
            if (uri!= null) iv.setImageURI(uri.toUri())
            name_tv.text = data?.getStringExtra("group31.lab1.FULL_NAME")
            nickname_tv.text = data?.getStringExtra("group31.lab1.NICKNAME")
            idCode_tv.text = data?.getStringExtra("group31.lab1.ID_CODE")
            email_tv.text = data?.getStringExtra("group31.lab1.EMAIL")
            phone_tv.text = data?.getStringExtra("group31.lab1.PHONE")
            location_tv.text = data?.getStringExtra("group31.lab1.LOCATION")
        }
    }
}