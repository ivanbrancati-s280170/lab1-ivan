package it.ivanbr.lab1

import android.app.Activity
import android.content.*
import android.graphics.Bitmap
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.core.view.drawToBitmap
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

//TODO: CROP IMAGE
class EditProfileActivity : AppCompatActivity(), PopupMenu.OnMenuItemClickListener {
    private lateinit var iv: ImageView
    private lateinit var name_tv: EditText
    private lateinit var nickname_tv: EditText
    private lateinit var idCode_tv: EditText
    private lateinit var email_tv: EditText
    private lateinit var phone_tv: EditText
    private lateinit var location_tv: EditText

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        //FOR SCROLL
        //setContentView(R.layout.try_scroll)

        iv = findViewById<ImageView>(R.id.editPhotoView)
        val uri = intent.getStringExtra("group31.lab1.PROFILE_PICTURE")
        if (uri != null) iv.setImageURI(uri.toUri())

        name_tv = findViewById<EditText>(R.id.editNameView)
        name_tv.setText(intent.getStringExtra("group31.lab1.FULL_NAME"))

        nickname_tv = findViewById<EditText>(R.id.editNicknameView)
        nickname_tv.setText(intent.getStringExtra("group31.lab1.NICKNAME"))

        idCode_tv = findViewById<EditText>(R.id.editIdCodeView)
        idCode_tv.setText(intent.getStringExtra("group31.lab1.ID_CODE"))

        email_tv = findViewById<EditText>(R.id.editEmailView)
        email_tv.setText(intent.getStringExtra("group31.lab1.EMAIL"))

        phone_tv = findViewById<EditText>(R.id.editPhoneView)
        phone_tv.setText(intent.getStringExtra("group31.lab1.PHONE"))

        location_tv = findViewById<EditText>(R.id.editLocationView)
        location_tv.setText(intent.getStringExtra("group31.lab1.LOCATION"))
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    fun showPictureMenu(v: View) {
        val popup = PopupMenu(this, v)
        popup.setOnMenuItemClickListener(this)
        val inflater: MenuInflater = popup.menuInflater
        inflater.inflate(R.menu.edit_picture_menu, popup.menu)
        popup.setForceShowIcon(true)
        popup.show()
    }
    /*fun showMenu(v: View) {
        PopupMenu(this, v).apply {
            // MainActivity implements OnMenuItemClickListener
            setOnMenuItemClickListener(this@MainActivity)
            inflate(R.menu.actions)
            show()
        }
    }*/

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.takePicture -> {
                //Open Camera
                dispatchTakePictureIntent()
                //Toast.makeText(applicationContext, "Camera", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.selectPicture -> {
                //Open Gallery
                pickImageFromGallery()
                //Toast.makeText(applicationContext, "Gallery", Toast.LENGTH_SHORT).show()
                true
            }
            else -> false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.save_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        setResult(Activity.RESULT_OK, Intent().also {
            it.putExtra(
                "group31.lab1.PROFILE_PICTURE", getSharedPreferences(
                    "picturePrefs", MODE_PRIVATE
                ).getString("imageUri", null)
            )
            it.putExtra("group31.lab1.FULL_NAME", name_tv.text.toString())
            it.putExtra("group31.lab1.NICKNAME", nickname_tv.text.toString())
            it.putExtra("group31.lab1.ID_CODE", idCode_tv.text.toString())
            it.putExtra("group31.lab1.EMAIL", email_tv.text.toString())
            it.putExtra("group31.lab1.PHONE", phone_tv.text.toString())
            it.putExtra("group31.lab1.LOCATION", location_tv.text.toString())

            val sharedPref = getSharedPreferences("profilePrefs", MODE_PRIVATE).edit()
            sharedPref.putString("name", name_tv.text.toString())
            sharedPref.putString("nickname", nickname_tv.text.toString())
            sharedPref.putString("idCode", idCode_tv.text.toString())
            sharedPref.putString("email", email_tv.text.toString())
            sharedPref.putString("phone", phone_tv.text.toString())
            sharedPref.putString("location", location_tv.text.toString())
            sharedPref.apply()

        })
        Toast.makeText(applicationContext, "Profile updated successfully!", Toast.LENGTH_SHORT)
            .show()
        finish()
        return true
    }

    val REQUEST_IMAGE_CAPTURE = 2
    private fun dispatchTakePictureIntent() {
        var takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            onActivityResult(REQUEST_IMAGE_CAPTURE, 1, takePictureIntent)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    // Function for thumbnail
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras!!.get("data") as Bitmap
            val imageURI = saveimage(imageBitmap)
            iv.setImageURI(imageURI)
            Toast.makeText(
                applicationContext,
                "Profile picture updated successfully!",
                Toast.LENGTH_SHORT
            ).show()
            val picturePref = getSharedPreferences("picturePrefs", MODE_PRIVATE).edit()
            picturePref.putString("imageUri", imageURI.toString())
            picturePref.apply()
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            val selectedImageUri: Uri? = data?.data
            iv.setImageURI(selectedImageUri)
            Toast.makeText(
                applicationContext,
                "Profile picture updated successfully!",
                Toast.LENGTH_SHORT
            ).show()
            val imageURI = saveimage(iv.drawToBitmap())
            val picturePref = getSharedPreferences("picturePrefs", MODE_PRIVATE).edit()
            picturePref.putString("imageUri", imageURI.toString())
            picturePref.apply()
        }

    }

    // Function for saving the image
    fun saveimage(bitmap: Bitmap): Uri {
        //Generating a file name
        val filename = "pro_pic_${SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())}.png"

        // Get the context wrapper instance
        val wrapper = ContextWrapper(applicationContext)

        // Initializing a new file
        var file = wrapper.getDir("images", Context.MODE_PRIVATE)

        // Create a file to save the image
        file = File(file, filename)

        try {
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(file)
            // Compress bitmap
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            // Flush the stream
            stream.flush()
            // Close stream
            stream.close()
        } catch (e: IOException) { // Catch the exception
            e.printStackTrace()
        }
        // Return the saved image uri
        return Uri.parse(file.absolutePath)
    }

    val REQUEST_IMAGE_GALLERY = 3
    private fun pickImageFromGallery() {
        //Intent to pick image
        val pickPictureIntent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media
                .EXTERNAL_CONTENT_URI
        )
        intent.type = "image/*"
        startActivityForResult(pickPictureIntent, REQUEST_IMAGE_GALLERY)
    }

    // Function for losing focus when touching outside the edit text view
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev?.action == MotionEvent.ACTION_DOWN) {
            val v: View? = currentFocus
            if (v is EditText) {
                var outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (ev != null) {
                    if (!outRect.contains(ev.getRawX().toInt(), ev.getRawY().toInt())) {
                        v.clearFocus()
                        var imm: InputMethodManager =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.windowToken, 0)
                    }

                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) iv.setImageURI(profilePicture.toUri())

    }

    /*override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) outState.putString("iv", profilePicture)
        outState.putString("ntv", name_tv.text.toString())
        outState.putString("nntv", nickname_tv.text.toString())
        outState.putString("itv", idCode_tv.text.toString())
        outState.putString("etv", email_tv.text.toString())
        outState.putString("ptv", phone_tv.text.toString())
        outState.putString("ltv", location_tv.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) iv.setImageURI(profilePicture.toUri())
        name_tv.text = savedInstanceState.getString("ntv").to
        nickname_tv.text = savedInstanceState.getString("nntv")
        idCode_tv.text = savedInstanceState.getString("itv")
        email_tv.text = savedInstanceState.getString("etv")
        phone_tv.text = savedInstanceState.getString("ptv")
        location_tv.text = savedInstanceState.getString("ltv")

    }*/

    /*override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) outState.putString("iv", profilePicture)
        outState.putString("ntv", name_tv.text.toString())
        outState.putString("nntv", nickname_tv.text.toString())
        outState.putString("itv", idCode_tv.text.toString())
        outState.putString("etv", email_tv.text.toString())
        outState.putString("ptv", phone_tv.text.toString())
        outState.putString("ltv", location_tv.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val profilePicture: String? = getSharedPreferences("picturePrefs",
                MODE_PRIVATE).getString("imageUri", null)
        if( profilePicture != null ) iv.setImageURI(profilePicture.toUri())
        name_tv.text = savedInstanceState.getString("ntv").to
        nickname_tv.text = savedInstanceState.getString("nntv")
        idCode_tv.text = savedInstanceState.getString("itv")
        email_tv.text = savedInstanceState.getString("etv")
        phone_tv.text = savedInstanceState.getString("ptv")
        location_tv.text = savedInstanceState.getString("ltv")

    }*/

}